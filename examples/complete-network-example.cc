/*
 * This script simulates a complex scenario with multiple gateways and end
 * devices. The metric of interest for this script is the throughput of the
 * network.
 */

#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/class-a-end-device-lorawan-mac.h"
#include "ns3/gateway-lorawan-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/node-container.h"
#include "ns3/mobility-helper.h"
#include "ns3/position-allocator.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/network-server-helper.h"
#include "ns3/correlated-shadowing-propagation-loss-model.h"
#include "ns3/building-penetration-loss.h"
#include "ns3/building-allocator.h"
#include "ns3/buildings-helper.h"
#include "ns3/forwarder-helper.h"
#include <algorithm>
#include <ctime>
/*
* Testar essas bibliotecas
*/
#include <ns3/okumura-hata-propagation-loss-model.h>
#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/simulator.h>
#include <iostream>
#include <fstream>
#include <string>
#include<stdlib.h>
#include <random>

//Netamin Module
//#include <ns3/netanim-module.h>

using namespace ns3;
using namespace lorawan;
// using namespace std;


NS_LOG_COMPONENT_DEFINE ("ComplexLorawanNetworkExample");

// Variables
bool FromFile = true; /*!< True if the positions is from a File */
std::string file_users = "teste/Sim/User_1000_Sim18.csv"; /*!< File that set *X* and *Y* of the users */
// std::string file_gateway = "teste/Sim/gw_1000_Best_Sim18.csv"; /*!< x (tab) y */
std::string file_gateway = "teste/gw.txt";
// Network settings
int nDevices = 3000;
int nDevices_extra = 0;
int nGateways = 1;
double radius = 3000;
double simulationTime = 1500;

// Channel model
bool realisticChannelModel = false;
int appPeriodSeconds = 300;

// Output control
bool print = true;
bool verbose = false;
uint32_t randomSeed = 1234;
uint32_t nRuns = 1;

//Changing to Square 
double axis_x = 10000;
double axis_y = 10000;

double averageArrival = 0.4;
double lamda = 1 / averageArrival;
std::mt19937 rng (0);
std::exponential_distribution<double> poi (lamda);
std::uniform_int_distribution<> dis(0, 9);
std::uniform_real_distribution<double> unif(0, 1);

double sumArrivalTimes=3.6;
double newArrivalTime;

double
poisson()
{
  newArrivalTime=poi.operator() (rng);// generates the next random number in the distribution 
  sumArrivalTimes=sumArrivalTimes + newArrivalTime;  
  // std::cout << "newArrivalTime:  " << newArrivalTime  << "    ,sumArrivalTimes:  " << sumArrivalTimes << std::endl;
  if (sumArrivalTimes<3.6)
  {
    sumArrivalTimes=3.6;
  }
  return sumArrivalTimes;
}



/**
 *    @name     getFileSize
 *    @brief    Read line by line and increment to calculate the number of line
 *    @param    std::string &fileName Name_of_File
 *    @date     05/23/19
 *    @todo     None 
 *    @bug      None
 *    @return   (int) how many lines the file has
 */
int getFileSize(const std::string &fileName)
{
	int numLines = 0;
	std::string unused;
	std::ifstream file(fileName.c_str());
	while ( std::getline(file, unused) )
		++numLines;
	return numLines;
}

/**
 * @name main (int argc, char *argv[])
 * @brief aram ui16_Address - First address to be read
 * @param ui16_Len - How many bytes to read
 * @todo None
 * @bug None
 * @return how many bytes it could write
 */
int
main (int argc, char *argv[])
{
	/**
	 * Set Values for specific variables from waf 
	 * ------------------------------------------
	 * @param var
	 * @parblock
	 *  Set a Variable use cmd.AddValue ("var", "Number of end devices to include in the simulation", var);
	 *
	 *  ./waf --run "complete-network-example var=X"
	 * @endparblock
	 */  
  CommandLine cmd;
  cmd.AddValue ("nDevices", "Number of end devices to include in the simulation", nDevices);
  cmd.AddValue ("radius", "The radius of the area to simulate", radius);
  cmd.AddValue ("simulationTime", "The time for which to simulate", simulationTime);
  cmd.AddValue ("appPeriod",
                "The period in seconds to be used by periodically transmitting applications",
                appPeriodSeconds);
  cmd.AddValue ("print", "Whether or not to print various informations", print);
	cmd.AddValue ("file_users","Name of User's File",file_users);
	cmd.AddValue ("file_gateway","Name of Gateway's File",file_gateway);  
	cmd.AddValue ("nRuns","Numbers of Silmulations",nRuns);    
  cmd.Parse (argc, argv);

  // Set up logging
  LogComponentEnable ("ComplexLorawanNetworkExample", LOG_LEVEL_ALL);
  if(verbose){
    LogComponentEnable("LoraChannel", LOG_LEVEL_INFO);
    LogComponentEnable("LoraPhy", LOG_LEVEL_ALL);
    LogComponentEnable("EndDeviceLoraPhy", LOG_LEVEL_ALL);
    LogComponentEnable("GatewayLoraPhy", LOG_LEVEL_ALL);
    LogComponentEnable("LoraInterferenceHelper", LOG_LEVEL_ALL);
    LogComponentEnable("LorawanMac", LOG_LEVEL_ALL);
    LogComponentEnable("EndDeviceLorawanMac", LOG_LEVEL_ALL);
    LogComponentEnable("ClassAEndDeviceLorawanMac", LOG_LEVEL_ALL);
    LogComponentEnable("GatewayLorawanMac", LOG_LEVEL_ALL);
    LogComponentEnable("LogicalLoraChannelHelper", LOG_LEVEL_ALL);
    LogComponentEnable("LogicalLoraChannel", LOG_LEVEL_ALL);
    LogComponentEnable("LoraHelper", LOG_LEVEL_ALL);
    LogComponentEnable("LoraPhyHelper", LOG_LEVEL_ALL);
    LogComponentEnable("LorawanMacHelper", LOG_LEVEL_ALL);
    LogComponentEnable("PeriodicSenderHelper", LOG_LEVEL_ALL);
    LogComponentEnable("PeriodicSender", LOG_LEVEL_ALL);
    LogComponentEnable("LorawanMacHeader", LOG_LEVEL_ALL);
    LogComponentEnable("LoraFrameHeader", LOG_LEVEL_ALL);
    LogComponentEnable("NetworkScheduler", LOG_LEVEL_ALL);
    LogComponentEnable("NetworkServer", LOG_LEVEL_ALL);
    LogComponentEnable("NetworkStatus", LOG_LEVEL_ALL);
    LogComponentEnable("NetworkController", LOG_LEVEL_ALL);
  }

	/**
	 *  Setup
	 *=======
	 * @parblock
	 * Create the time value from the period and set a seed
	 *
	 * Decides how the devices are created
	 * ------------------------------------
	 * @endpar
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
	 * if( !FromFile ) 
	 *     Create devices random
	 * else
	 *     Import positions from file_users
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */

  // Create the time value from the period
  Time appPeriod = Seconds (appPeriodSeconds);

  // Mobility
	for (uint32_t i = 1; i <= nRuns; i++) {
		uint32_t seed = randomSeed * i;
		SeedManager::SetSeed (seed);
    rng.seed(seed);    
	  MobilityHelper mobility;
    if ( FromFile ) {
      // // Mobility Disco like
      // mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
      //                               "rho", DoubleValue (radius),
      //                               "X", DoubleValue (0.0),
      //                               "Y", DoubleValue (0.0));
      // Mobility Disco like
      mobility.SetPositionAllocator ("ns3::RandomRectanglePositionAllocator",
              "X", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=10000.0]"),  
              "Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=10000.0]"));                                
    } else {
      Ptr<ListPositionAllocator> allocat = CreateObject<ListPositionAllocator> ();
      //Mobility Square Like
      std::ifstream infile(file_users);
      double a, b;
      while(infile >> a >> b)
      {
      	// std::cout << "x: " << a << ", y: "<< b << std::endl;
        allocat->Add (Vector (a,b,0.0));
      }
      infile.close();
      mobility.SetPositionAllocator(allocat);
    }
    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

    /************************
     *  Create the channel  *
     ************************/

    // Create the lora channel object
    Ptr<OkumuraHataPropagationLossModel> loss = CreateObject<OkumuraHataPropagationLossModel> ();
    // Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
    // loss->SetPathLossExponent (3.76);
    // loss->SetReference (1, 7.7);

		/**
		 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
		 * if(realisticChannelModel) {
		 *   Create the correlated shadowing component
		 *   Aggregate shadowing to the Okumura Hata loss
		 *   Add the effect to the channel propagation loss
		 * } 
		 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		 */
    if (realisticChannelModel)
    {
      // Create the correlated shadowing component
      Ptr<CorrelatedShadowingPropagationLossModel> shadowing =
          CreateObject<CorrelatedShadowingPropagationLossModel> ();

      // Aggregate shadowing to the logdistance loss
      loss->SetNext (shadowing);
      // Add the effect to the channel propagation loss
      Ptr<BuildingPenetrationLoss> buildingLoss = CreateObject<BuildingPenetrationLoss> ();
      shadowing->SetNext (buildingLoss);
    }
    
    Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();
    Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);

    /************************
     *  Create the helpers  *
     ************************/

    // Create the LoraPhyHelper
    LoraPhyHelper phyHelper = LoraPhyHelper ();
    phyHelper.SetChannel (channel);

    // Create the LorawanMacHelper
    LorawanMacHelper macHelper = LorawanMacHelper ();

    // Create the LoraHelper
    LoraHelper helper = LoraHelper ();
    helper.EnablePacketTracking (); // Output filename
    // helper.EnableSimulationTimePrinting ();

    //Create the NetworkServerHelper
    NetworkServerHelper nsHelper = NetworkServerHelper ();

    //Create the ForwarderHelper
    ForwarderHelper forHelper = ForwarderHelper ();

    /************************
     *  Create End Devices  *
     ************************/

    // Create a set of nodes
    NodeContainer endDevices;
    // NodeContainer endDevices2;
		if (FromFile) {
			endDevices.Create (nDevices+nDevices_extra);
			// endDevices2.Create (nDevices_extra);      
    } else {
			endDevices.Create ( getFileSize(file_users) );
    }
    // Assign a mobility model to each node
    mobility.Install (endDevices);
    // mobility.Install (endDevices2);    

    // Make it so that nodes are at a certain height > 0
    for (NodeContainer::Iterator j = endDevices.Begin (); 
        j != endDevices.End (); ++j)
      {
        Ptr<MobilityModel> mobility = (*j)->GetObject<MobilityModel> ();
        Vector position = mobility->GetPosition ();
        position.z = 1.2;
        mobility->SetPosition (position);
      }    

    // Create the LoraNetDevices of the end devices
    uint8_t nwkId = 54;
    uint32_t nwkAddr = 1864;
    Ptr<LoraDeviceAddressGenerator> addrGen =
        CreateObject<LoraDeviceAddressGenerator> (nwkId, nwkAddr);

    // Create the LoraNetDevices of the end devices
    macHelper.SetAddressGenerator (addrGen);
    phyHelper.SetDeviceType (LoraPhyHelper::ED);
    macHelper.SetDeviceType (LorawanMacHelper::ED_A);
    helper.Install (phyHelper, macHelper, endDevices);
    // helper.Install (phyHelper, macHelper, endDevices2);

    // Now end devices are connected to the channel

    // Connect trace sources
    for (NodeContainer::Iterator j = endDevices.Begin (); j != endDevices.End (); ++j)
      {
        Ptr<Node> node = *j;
        Ptr<LoraNetDevice> loraNetDevice = node->GetDevice (0)->GetObject<LoraNetDevice> ();
        Ptr<LoraPhy> phy = loraNetDevice->GetPhy ();
      }    

    /*********************
     *  Create Gateways  *
     *********************/

    // Create the gateway nodes (allocate them uniformely on the disc)
    NodeContainer gateways;
    gateways.Create (nGateways);

    Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
    // Make it so that nodes are at a certain height > 0
    // allocator->Add (Vector (0.0, 0.0, 15.0));
	if ( !FromFile ) {
		gateways.Create (nGateways);
		if(nGateways == 1) {
			allocator->Add (Vector (0.0, 0.0, 15.0));
		} else if (nGateways == 2){
			allocator->Add (Vector (-1500.0, 000.0, 15.0));
			allocator->Add (Vector (1500.0, 000.0, 15.0));
		}else {
      // randomize();
			for(int i=1;i<= nGateways;i++)
			{
				allocator->Add (Vector (rand()%10000, rand()%10000, 15.0));
			}
		}
	} else {
		gateways.Create ( getFileSize(file_gateway) );
		// Get Gateway positions from File
		std::ifstream infile2(file_gateway);
		double gwa, gwb;
		while(infile2 >> gwa >> gwb)
		{
//			cout << "Gx: " << gwa*1000 << ", Gy: "<< gwb*1000 << endl;
//			allocator->Add (Vector (gwa,gwb,15.0));
			allocator->Add (Vector (gwa,gwb,15.0));
		}
		infile2.close();
	}



    mobility.SetPositionAllocator (allocator);
    mobility.Install (gateways);

    // Create a netdevice for each gateway
    phyHelper.SetDeviceType (LoraPhyHelper::GW);
    macHelper.SetDeviceType (LorawanMacHelper::GW);
    helper.Install (phyHelper, macHelper, gateways);

    /**********************
     *  Handle buildings  *
     **********************/

    double xLength = 130;
    double deltaX = 32;
    double yLength = 64;
    double deltaY = 17;
    int gridWidth = 2 * radius / (xLength + deltaX);
    int gridHeight = 2 * radius / (yLength + deltaY);
    if (realisticChannelModel == false)
      {
        gridWidth = 0;
        gridHeight = 0;
      }
    Ptr<GridBuildingAllocator> gridBuildingAllocator;
    gridBuildingAllocator = CreateObject<GridBuildingAllocator> ();
    gridBuildingAllocator->SetAttribute ("GridWidth", UintegerValue (gridWidth));
    gridBuildingAllocator->SetAttribute ("LengthX", DoubleValue (xLength));
    gridBuildingAllocator->SetAttribute ("LengthY", DoubleValue (yLength));
    gridBuildingAllocator->SetAttribute ("DeltaX", DoubleValue (deltaX));
    gridBuildingAllocator->SetAttribute ("DeltaY", DoubleValue (deltaY));
    gridBuildingAllocator->SetAttribute ("Height", DoubleValue (6));
    gridBuildingAllocator->SetBuildingAttribute ("NRoomsX", UintegerValue (2));
    gridBuildingAllocator->SetBuildingAttribute ("NRoomsY", UintegerValue (4));
    gridBuildingAllocator->SetBuildingAttribute ("NFloors", UintegerValue (2));
    gridBuildingAllocator->SetAttribute (
        "MinX", DoubleValue (-gridWidth * (xLength + deltaX) / 2 + deltaX / 2));
    gridBuildingAllocator->SetAttribute (
        "MinY", DoubleValue (-gridHeight * (yLength + deltaY) / 2 + deltaY / 2));
    BuildingContainer bContainer = gridBuildingAllocator->Create (gridWidth * gridHeight);

    BuildingsHelper::Install (endDevices);
    BuildingsHelper::Install (gateways);
    BuildingsHelper::MakeMobilityModelConsistent ();

    // Print the buildings
    if (print)
      {
        std::ofstream myfile;
        myfile.open ("src/lorawan/examples/buildings.txt");
        std::vector<Ptr<Building>>::const_iterator it;
        int j = 1;
        for (it = bContainer.Begin (); it != bContainer.End (); ++it, ++j)
          {
            Box boundaries = (*it)->GetBoundaries ();
            myfile << "set object " << j << " rect from " << boundaries.xMin << "," << boundaries.yMin
                  << " to " << boundaries.xMax << "," << boundaries.yMax << std::endl;
          }
        myfile.close ();
      }

    /**********************************************
     *  Set up the end device's spreading factor  *
     **********************************************/

    macHelper.SetSpreadingFactorsUp (endDevices, gateways, channel);
    // macHelper.SetSpreadingFactorsUp (endDevices2, gateways, channel);    

    // NS_LOG_DEBUG ("Completed configuration");

    /*********************************************
     *  Install applications on the end devices  *
     *********************************************/  
    Time appStopTime = Seconds (simulationTime);
    PeriodicSenderHelper appHelper = PeriodicSenderHelper ();
    appHelper.SetPeriod (Seconds (appPeriodSeconds));
    appHelper.SetPacketSize (20);    
    // Ptr<RandomVariableStream> rv = CreateObjectWithAttributes<UniformRandomVariable> (
    //     "Min", DoubleValue (12), "Max", DoubleValue (23));
    // appHelper.SetPacketSizeRandomVariable(rv);        
    ApplicationContainer appContainer = appHelper.Install (endDevices);
    // ApplicationContainer appContainer2 = appHelper.Install (endDevices);
    // appContainer2.Start (Seconds (0));
    for (uint i = 0; i < appContainer.GetN(); i++) {
      if( (int)i < nDevices) {
        // std::cout << "I " << i << std::endl;
	      Ptr<Application> app = appContainer.Get(i); // Pega a aplicação de um device apenas 
        app->SetStartTime (Seconds(0)); // Coloca o start time de apenas um
      } else {
        // std::cout << "I " << i << std::endl;
        Ptr<Application> app = appContainer.Get(i); // Pega a aplicação de um device apenas 
        double t=poisson(); // Chama a função
        app->SetStartTime (Seconds(t)); // Coloca o start time de apenas um
      }
    }

    appContainer.Stop (appStopTime); // Coloca um tempo final para todos
    // appContainer2.Stop (appStopTime); // Coloca um tempo final para todos

    // Time appStopTime = Seconds (simulationTime);
    // PeriodicSenderHelper appHelper = PeriodicSenderHelper ();
    // appHelper.SetPeriod (Seconds (appPeriodSeconds));
    // appHelper.SetPacketSize (23);
    // Ptr<RandomVariableStream> rv = CreateObjectWithAttributes<UniformRandomVariable> (
    //     "Min", DoubleValue (0), "Max", DoubleValue (10));
    // ApplicationContainer appContainer = appHelper.Install (endDevices);

    // appContainer.Start (Seconds (0));
    // appContainer.Stop (appStopTime);

    /**************************
     *  Create Network Server  *
     ***************************/

    // Create the NS node
    NodeContainer networkServer; 
    networkServer.Create (1);

    // Create a NS for the network
    nsHelper.SetEndDevices (endDevices);
    nsHelper.SetGateways (gateways);
    nsHelper.Install (networkServer);

    //Create a forwarder for each gateway
    forHelper.Install (gateways);

    // helper.PrintEndDevices (endDevices, gateways,"src/lorawan/examples/endDevices.dat");
    helper.DoPrintDeviceStatus (endDevices, gateways,"src/lorawan/examples/endDevices.dat");    

    ////////////////
    // Simulation //
    ////////////////

    Simulator::Stop (appStopTime + Hours (1));

    // NS_LOG_INFO ("Running simulation...");
    Simulator::Run ();

    Simulator::Destroy ();

    ///////////////////////////
    // Print results to file //
    ///////////////////////////
    // NS_LOG_INFO ("Computing performance metrics...");

    LoraPacketTracker &tracker = helper.GetPacketTracker ();
    std::cout << tracker.CountMacPacketsGlobally (Seconds (0), appStopTime + Hours (1)) << std::endl;
  }
  return 0;
}
